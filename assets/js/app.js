$(document).ready(function() {
    var templates = {
        'default': {
            'issuer':    '',
            'user':      '',
            'secret':    '',
            'image':     '',
            'type':      'totp',
            'digits':    6,
            'algorithm': 'SHA1',
            'period':    30,
            'counter':   0
        },
        'binance': {
            'issuer': 'Binance',
            'image':  'https://resource.binance.com/resources/img/binance_icon.png'
        },
        'cyber': {
            'issuer': 'Cyber',
            'image':  'https://forum.whoosis.fr/uploads/default/original/1X/37de9252a2456b4b82b89351639c68a1fe035813.PNG'
        },
        'hitbtc': {
            'issuer': 'HitBTC',
            'image':  'https://hitbtc.com/195x195image.png'
        },
        'kraken': {
            'issuer': 'Kraken',
            'image':  'https://themerkle.com/wp-content/uploads/2017/01/kraken-logo-300x300.png',
            'digits': 8,
            'algorithm': 'SHA512'
        },
        'blizzard': {
            'issuer': 'Blizzard',
            'image':  'https://us.battle.net/forums/static/images/logos/logo-small-bnet.png',
            'digits': 8
        },
        'facebook': {
            'issuer': 'Facebook',
            'image':  'https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png'
        },
        'snapchat': {
            'issuer': 'Snapchat',
            'image':  'https://lh4.ggpht.com/vdK_CsMSsJoYvJpYgaj91fiJ1T8rnSHHbXL0Em378kQaaf_BGyvUek2aU9z2qbxJCAFV=w300'
        },
        'twitter': {
            'issuer': 'Twitter',
            'image':  'https://abs.twimg.com/icons/apple-touch-icon-192x192.png'
        },
        'gmail': {
            'issuer': 'Gmail',
            'image':  'https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/New_Logo_Gmail.svg/256px-New_Logo_Gmail.svg.png'
        },
        'mastodon': {
            'issuer': 'Mastodon',
            'image':  'https://raw.githubusercontent.com/tootsuite/mastodon/master/public/apple-touch-icon.png'
        },
        'github': {
            'issuer': 'GitHub',
            'image':  'https://assets-cdn.github.com/images/modules/logos_page/GitHub-Mark.png'
        },
        'ovh': {
            'issuer': 'OVH',
            'image':  'https://www.ovh.com/images/totp/ovh.png'
        },
        'gitlab': {
            'issuer': 'GitLab',
            'image':  'https://about.gitlab.com/ico/favicon-192x192.png'
        }
    },
    args = {
        issuerInput:      '#otp-issuer',
        userInput:        '#otp-user',
        secretInput:      '#otp-secret',
        imageInput:       '#otp-image',
        typeInput:        '#otp-type',
        digitsInput:      '#otp-digits',
        algorithmInput:   '#otp-algorithm',
        periodInput:      '#otp-period',
        counterInput:     '#otp-counter',
        templateSelector: '#otp-template',
        form:             '#mainForm',
        output:           '#output',
        previewOutput:    '#otp-preview',
        qrOutput:         '#otp-qr',
        urlOutput:        '#otp-url'
    };

    $.each(templates, function(k,v){
        if (k !== 'default') {
            $(args.templateSelector).append(
                $('<option>', {
                    'value': k,
                    'text': v.issuer
                })
            );
        }
    });

    $(args.templateSelector).change(function() {
        var template = $(this).val();
        if (template !== '' && template in templates) {
            document.querySelector(args.issuerInput).value    = templates[template].issuer    || templates.default.issuer;
            document.querySelector(args.imageInput).value     = templates[template].image     || templates.default.image;
            document.querySelector(args.digitsInput).value    = templates[template].digits    || templates.default.digits;
            document.querySelector(args.algorithmInput).value = templates[template].algorithm || templates.default.algorithm;
            document.querySelector(args.periodInput).value    = templates[template].period    || templates.default.period;
            document.querySelector(args.counterInput).value   = templates[template].counter   || templates.default.counter;
            $(args.typeInput).val(templates[template].type || templates.default.type);
        }
    });

    $(args.typeInput).change(function() {
        var type = $(this).val();
        if (type === 'totp') {
            $(args.periodInput).css('display', 'inline-block');
            $(args.counterInput).css('display', 'none');
        } else {
            $(args.periodInput).css('display', 'none');
            $(args.counterInput).css('display', 'inline-block');
        }
    });

    $(args.form).submit(function(event) {
        event.preventDefault();

        var url = generateOTPURL(args);

        var options = {
            render:     'image',
            ecLevel:    'H',
            minVersion: 10,
            fill:       '#3e3e3e',
            background: '#f9f9f9',
            text:       url,
            size:       400,
            radius:     0.5,
            quiet:      4
        };

        document.querySelector(args.previewOutput + "-issuer").textContent = (document.querySelector(args.issuerInput).value                || templates.default.issuer);
        document.querySelector(args.previewOutput + "-user").textContent   = (document.querySelector(args.userInput).value                  || templates.default.user);
        document.querySelector(args.previewOutput + "-code").textContent   = Array(parseInt((document.querySelector(args.digitsInput).value || templates.default.digits), 10) + 1).join('\u2013');
        $(args.previewOutput + "-image").attr('xlink:href',                  (document.querySelector(args.imageInput).value                 || templates.default.image));

        $(args.urlOutput).val(url).prop('type', 'text');
        $(args.qrOutput).empty().qrcode(options).css('display', 'inline-block');
        $(args.previewOutput).css('display', 'inline-block');

        $('html, body').animate({
            scrollTop: $(args.output).offset().top
        }, 100);

        return false;
    });

    function generateOTPURL(args) {
        var issuer    = (document.querySelector(args.issuerInput).value    || templates.default.issuer),
            user      = (document.querySelector(args.userInput).value      || templates.default.user),
            secret    = (document.querySelector(args.secretInput).value    || templates.default.secret),
            image     = (document.querySelector(args.imageInput).value     || templates.default.image),
            type      = ($(args.typeInput).val()                           || templates.default.type),
            digits    = (document.querySelector(args.digitsInput).value    || templates.default.digits),
            algorithm = (document.querySelector(args.algorithmInput).value || templates.default.algorithm),
            period    = (document.querySelector(args.periodInput).value    || templates.default.period),
            counter   = (document.querySelector(args.counterInput).value   || templates.default.counter),
            url       = '';

        url =  'otpauth://';
        url += type;
        url += '/' + encodeURIComponent(issuer);
        url += ':' + encodeURIComponent(user);
        url += '?secret=' + secret.replace(/[-\s\t]/g,'').toUpperCase();
        url += '&issuer=' + encodeURIComponent(issuer);
        url += '&digits=' + digits;
        url += '&algorithm=' + algorithm;
        url += (type === 'totp') && '&period=' + period || '&counter=' + counter;
        url += (image !== '') && '&image=' + image || '';

        return url
    }
});
